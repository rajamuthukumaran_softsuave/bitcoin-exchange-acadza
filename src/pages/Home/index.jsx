import React, { useState } from 'react'
import { Container, Grid } from '@material-ui/core'
import Header from '../../components/Header'
import './style.scss'
import Navigation from '../../components/Navigation'
import Accuracy from '../Accuracy'
import clsx from 'clsx'

const Home = () => {
  const [isMenu, setMenu] = useState(false)

  return (
    <div className="home-container">
      <Header isMenu={isMenu} setMenu={setMenu} />

      <Grid container className="home-grid-wrap">
        {/* prettier-ignore */}
        <Grid item xs={12} lg={2} className={clsx("navigation-grid", isMenu && "menu-open")}>
          <Navigation isMenu={isMenu} setMenu={setMenu}/>
        </Grid>

        <Grid item xs={12} lg={10} className="pages-grid">
          <Container maxWidth="xl" className="pages-container">
            <Accuracy />
          </Container>
        </Grid>
      </Grid>
    </div>
  )
}

export default Home
