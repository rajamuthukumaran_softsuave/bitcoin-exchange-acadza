import React, { useEffect, useState } from 'react'
import CustomDropdown from '../../components/CustomDropdown'
import useFetch from '../../hooks/useFetch'
import Graph from './Graph'
import './style.scss'

const Accuracy = () => {
  const coinOptions = [
    { id: 'BTC', value: 'Bitcoin' },
    { id: 'ETH', value: 'Ether' },
    { id: 'LTC', value: 'Litecoin' },
    { id: 'ZEC', value: 'Zcash' },
    { id: 'VTC', value: 'Verticoin' },
    { id: 'XLM', value: 'Stellar Lumen' }
  ]

  const [currencyA, setCurrencyA] = useState(coinOptions[0].id)
  const [currencyB, setCurrencyB] = useState(coinOptions[1].id)

  const [datasetA, setDatasetA] = useState([])
  const [datasetB, setDatasetB] = useState([])

  const apiUrl = 'https://min-api.cryptocompare.com/data/price'

  const { fetchDataA, dataA } = useFetch({
    url: apiUrl,
    name: 'dataA',
    params: {
      fsym: currencyA,
      tsyms: 'USD'
    },
    initLoad: true
  })

  const { fetchDataB, dataB } = useFetch({
    url: apiUrl,
    name: 'dataB',
    params: {
      fsym: currencyB,
      tsyms: 'USD'
    },
    initLoad: true
  })

  useEffect(() => {
    setDatasetA([])

    const refreshDataA = setInterval(() => {
      fetchDataA()
    }, 1000)

    return () => clearInterval(refreshDataA)
  }, [currencyA])

  useEffect(() => {
    setDatasetB([])

    const refreshDataB = setInterval(() => {
      fetchDataB()
    }, 1000)

    return () => clearInterval(refreshDataB)
  }, [currencyB])

  useEffect(() => {
    !!dataA?.USD &&
      setDatasetA((prev) => {
        if (prev.length > 10) {
          let newList = prev.filter((f, index) => !!index)
          return [...newList, dataA?.USD]
        }

        return [...prev, dataA?.USD]
      })
  }, [dataA])

  useEffect(() => {
    !!dataB?.USD &&
      setDatasetB((prev) => {
        if (prev.length > 10) {
          let newList = prev.filter((f, index) => !!index)
          return [...newList, dataB?.USD]
        }

        return [...prev, dataB?.USD]
      })
  }, [dataB])

  return (
    <section className="accuracy-sec-wrap">
      <div className="info-header-wrap">
        <div className="left-part">
          <img
            className="bitcoin-icon"
            src={require('../../assets/img/bitcoin.svg').default}
            alt="bitcoin"
          />
          <span className="graph-name">Real-time bitcoin graph</span>
        </div>

        <div className="right-part">
          <div className="currency-options">
            <CustomDropdown
              className="cur-sel cur-a"
              popClassName="cur-sel-dropdown"
              list={coinOptions}
              value={currencyA}
              setChange={setCurrencyA}
              primary
            />
            <CustomDropdown
              className="cur-sel cur-b"
              popClassName="cur-sel-dropdown"
              list={coinOptions}
              value={currencyB}
              setChange={setCurrencyB}
              secondary
            />
          </div>
        </div>
      </div>

      <div className="graph-container">
        <Graph
          datasetA={datasetA}
          datasetB={datasetB}
          currencyA={currencyA}
          currencyB={currencyB}
        />
      </div>
    </section>
  )
}

export default Accuracy
