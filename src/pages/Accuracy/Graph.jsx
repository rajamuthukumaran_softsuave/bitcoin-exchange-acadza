import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

function Graph({ datasetA, datasetB, currencyA, currencyB }) {
  const config = {
    chart: {
      type: 'spline',
      marginRight: 10,
      backgroundColor: '#FFFDF7'
    },

    title: {
      text: ''
    },

    credits: {
      enabled: false
    },

    legend: {
      enabled: false
    },

    exporting: {
      enabled: false
    },

    plotOptions: {
      series: {
        pointStart: 0,
        pointWidth: 20,
        borderColor: null,
        states: {
          inactive: {
            enabled: false
          }
        }
      }
    },

    xAxis: {
      tickWidth: 0,
      lineWidth: 3,
      lineColor: '#BCBCBC',
      labels: {
        enabled: false
      },
      gridLineWidth: 1,
      min: 0
    },

    yAxis: {
      gridLineWidth: 0,
      lineWidth: 3,
      lineColor: '#BCBCBC',
      title: {
        text: ''
      },
      gridLineWidth: 1
    },

    series: [
      {
        name: currencyA,
        data: datasetA,
        color: '#5843BE'
      },
      {
        name: currencyB,
        data: datasetB,
        marker: {
          symbol: 'circle'
        },
        color: '#C30F70'
      }
    ]
  }

  return (
    <div className="graph-wrap">
      <div className="graph-legends">
        <div className="lgnd lgnd-1">
          <div className="color"></div>
          <p className="entry">Data set 1</p>
        </div>

        <div className="lgnd lgnd-2">
          <div className="color"></div>
          <p className="entry">Data set 2</p>
        </div>
      </div>
      <HighchartsReact highcharts={Highcharts} options={config} />
    </div>
  )
}

export default Graph
