import React from 'react'
import { Popover, MenuItem } from '@material-ui/core'
import clsx from 'clsx'

const CustomPopover = ({ list, value, valueid, displayid, handleChange, setAnchorEl, anchorEl, primary, secondary, popClassName }) => {
    return (
        <Popover
            open={Boolean(anchorEl)}
            className={clsx('custom-dd-popover', popClassName, {
                primary: !!primary,
                secondary: !!secondary
            })}
            anchorEl={anchorEl}
            onClose={() => setAnchorEl(null)}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center'
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center'
            }}
        >
            {list &&
                list.map((i) => (
                    <MenuItem
                        className={
                            'dd-menu-item' + (i[valueid] === value ? ' active' : '')
                        }
                        key={i[valueid]}
                        value={i[valueid]}
                        onClick={() => handleChange(i)}
                    >
                        {i[displayid]}
                    </MenuItem>
                ))}
        </Popover>
    )
}
export default CustomPopover