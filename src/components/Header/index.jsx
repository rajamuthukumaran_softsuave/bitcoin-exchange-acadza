import { IconButton } from '@material-ui/core'
import { Search, Menu } from '@material-ui/icons'
import clsx from 'clsx'
import React from 'react'
import IconMenu from '../IconMenu'
import Textfield from '../Textfield'
import TextMenu from '../TextMenu'
import './style.scss'

const activityOptions = [
  { id: '1', value: 'Activity 1' },
  { id: '2', value: 'Activity 2' },
  { id: '3', value: 'Activity 3' },
  { id: '4', value: 'Activity 4' },
  { id: '5', value: 'Activity 5' }
]

const toolGuideOptions = [
  { id: '1', value: 'Tool Guide 1' },
  { id: '2', value: 'Tool Guide 2' },
  { id: '3', value: 'Tool Guide 3' },
  { id: '4', value: 'Tool Guide 4' },
  { id: '5', value: 'Tool Guide 5' }
]

const profileMenuOptions = [
  { id: '1', value: 'My profile' },
  { id: '2', value: 'Settings' },
  { id: '3', value: 'Logout' }
]

// header dropdown options component
const DDOptions = ({ activityOptions, toolGuideOptions, className }) => (
  <div className={clsx('dd-options-wrap', 'item-bg-shade', className)}>
    <div className="dd-wrap dd-1">
      <TextMenu
        text="Activity"
        className="dd-act"
        list={activityOptions}
        primary
      />
    </div>
    <div className="dd-wrap dd-2">
      <TextMenu
        text="Tool Guide"
        className="dd-tool"
        list={toolGuideOptions}
        primary
      />
    </div>
  </div>
)

// search bar component
const SearchBar = ({ className }) => (
  <div className={clsx('search-wrap', className)}>
    <Textfield
      className="search-bar"
      placeholder="Search..."
      prefix={<Search />}
    />
  </div>
)

const Header = ({ setMenu }) => {

  const ddProps = { activityOptions, toolGuideOptions }

  return (
    <div className="header-wrap">
      <div className="header-container">
        <div className="left-partition partition">
          <div className="menu-btn-wrap">
            <IconButton
              className="menu-btn"
              onClick={() => setMenu((prev) => !prev)}
            >
              <Menu className="menu-btn-icon" />
            </IconButton>
          </div>

          <div className="logo-wrap">
            <img
              src={require('../../assets/img/logo.svg').default}
              alt="Acadza"
            />
          </div>

          <DDOptions {...ddProps} />
        </div>

        <div className="right-partition partition">
          <SearchBar />

          <div className="notification-panel item-bg-shade">
            <div className="not-menu-wrap">
              <IconButton className="notification-menu" title="notification">
                <img
                  className="notification-icon"
                  src={require('../../assets/img/notification.svg').default}
                  alt="Notification"
                />
              </IconButton>
            </div>

            <div className="prof-menu-wrap">
              <IconMenu
                className="profile-menu"
                icon={
                  <img
                    className="prof-img"
                    src={require('../../assets/img/profile.jpg').default}
                    alt="person"
                  />
                }
                list={profileMenuOptions}
                showArrow
              />
            </div>
          </div>
        </div>
      </div>

      <div className="mobile-options">
        <DDOptions {...ddProps} className="is-mobile" />
        <SearchBar className="is-mobile" />
      </div>
    </div>
  )
}

export default Header
