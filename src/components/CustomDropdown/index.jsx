import React, { useState } from 'react'
import { ArrowDropDown } from '@material-ui/icons'
import clsx from 'clsx'
import CustomPopover from '../CustomPopover'
import './style.scss'

const CustomDropdown = ({
  name,
  display,
  value,
  valueId,
  list,
  className,
  popClassName,
  onChange,
  setChange,
  primary,
  secondary
}) => {
  const [anchorEl, setAnchorEl] = useState(null)
  const valueid = !!valueId ? valueId : 'id'
  const displayid = !!display ? display : 'value'

  const sortName = (id) => {
    const valueFilter = list?.filter((i) => i[valueid] === id)
    return valueFilter?.length > 0 ? valueFilter[0]?.[displayid] : ''
  }

  const handleChange = (data) => {
    setAnchorEl(null)
    !!onChange &&
      onChange({
        target: {
          name,
          value: data[valueid]
        }
      })
    !!setChange && setChange(data[valueid])
  }

  return (
    <>
      <div
        className={clsx('custom-dropdown', className, {
          'custom-dd-open': Boolean(anchorEl),
          'custom-dd-empty': !value,
          primary: !!primary,
          secondary: !!secondary
        })}
        onClick={(e) => setAnchorEl(e.currentTarget)}
      >
        <span className="dropdown-content">
          <span className="dropdown-selected">{sortName(value)}</span>
          <span className="dd-icon">
            <ArrowDropDown />
          </span>
        </span>
      </div>
      <CustomPopover
        list={list}
        value={value}
        valueid={valueid}
        displayid={displayid}
        handleChange={handleChange}
        setAnchorEl={setAnchorEl}
        anchorEl={anchorEl}
        primary={primary}
        secondary={secondary}
        popClassName={popClassName}
      />
    </>
  )
}

export default CustomDropdown
