export const menuList = [
  {
    name: 'Dashboard',
    path: '',
    icon: require('../../assets/img/dashboard.svg').default
  },
  {
    name: 'Backlog Remover',
    path: '',
    icon: require('../../assets/img/backlog.svg').default
  },
  {
    name: 'Rank up',
    path: '',
    icon: require('../../assets/img/rank.svg').default
  },
  {
    name: 'Speed up',
    path: '',
    icon: require('../../assets/img/speed.svg').default
  },
  {
    name: 'Accuracy up',
    path: '',
    icon: require('../../assets/img/accuracy.svg').default
  },
  {
    name: 'Revision',
    path: '',
    icon: require('../../assets/img/revision.svg').default
  },
  {
    name: 'Test Creator',
    path: '',
    icon: require('../../assets/img/test.svg').default
  },
  {
    name: 'Assignment Creator',
    path: '',
    icon: require('../../assets/img/assignment.svg').default
  },
  {
    name: 'Study Material',
    path: '',
    icon: require('../../assets/img/study.svg').default
  },
  {
    name: 'Formula Sheet',
    path: '',
    icon: require('../../assets/img/rank.svg').default
  }
]
