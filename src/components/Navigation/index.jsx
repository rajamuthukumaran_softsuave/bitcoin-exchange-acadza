import React from 'react'
import {
  Button,
  List,
  ListItem,
  ListItemIcon,
  ListItemText
} from '@material-ui/core'
import { menuList } from './menuList'
import clsx from 'clsx'
import './style.scss'

const MenuList = ({ data, isSelected }) => (
  <ListItem button className={clsx('menu-item', isSelected && 'selected')}>
    <ListItemIcon className="menu-icon">
      <img src={data.icon} alt="menu icon" />
    </ListItemIcon>
    <ListItemText primary={data.name} className="menu-name" />
  </ListItem>
)

const Navigation = () => {
  return (
    <div className="navigation-wrap">
      <div className="tool-btn-wrap">
        <Button className="tool-btn">
          <img
            className="tool-icon"
            src={require('../../assets/img/tool.svg').default}
            alt="tool"
          />
          Tools
        </Button>
      </div>

      <div className="menu-list">
        <List component="nav" className="menu-top-options">
          {menuList?.map((i, index) => (
            <MenuList
              key={index}
              data={i}
              isSelected={i.name === 'Accuracy up'}
            />
          ))}
        </List>
      </div>
    </div>
  )
}

export default Navigation
