export const commonDateFmt = (date) => {
  const moment = require("moment");
  return !!date
    ? moment(new Date(date), "YYYY-MM-DD").format("MMMM DD, YYYY")
    : "";
};

export const numberFmt = (number) => {
  return Intl.NumberFormat("en-US").format(parseFloat(number));
};

export const capitalize = (value) => {
  return value.charAt(0).toUpperCase() + value.slice(1);
};
