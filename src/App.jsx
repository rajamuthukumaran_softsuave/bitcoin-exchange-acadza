import React from 'react'
import { Container, Grid } from '@material-ui/core'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import Home from './pages/Home'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#5843BE'
    },
    secondary: {
      main: '#C30F70'
    }
  },
  typography: {
    fontFamily: 'Poppins, sans-serif',
    textTransform: 'none'
  },
  button: {
    textTransform: 'none'
  }
})

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Home />
    </ThemeProvider>
  )
}

export default App
